# Fonctionnement
Plutôt que d'enregistrer l'image sur un serveur, ce module encode l'image en base 64 et l'envoi en HTML dans la propriété `src` d'un tag `img`. Vos images sont donc uniquement envoyer par courriel et ne seront jamais entreposées sur un service public où tout le monde pourrait y avoir accès.

# Notes
Certains services de courriels ne fonctionnent pas avec cette application. C'est le cas de Outlook et GMail. Vous pouvez facilement tester cette application avec différents services de courriels, mais, pour vous économiser du temps, sachez que [ProtonMail](https://protonmail.com/) fonctionne à merveille.

# TODO
Utiliser le content type reçu du serveur pour le content type de l'image envoyée.
