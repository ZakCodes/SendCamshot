// ----------------------------------------------------------------------------
// --- Class definition, inheritance and setup
// ----------------------------------------------------------------------------

/**
    Créer une nouvelle instance de l'application.
    @constructor
    @param {int} id
    @param {any} controller
*/
function SendCamshot(id, controller) {
    SendCamshot.super_.call(this, id, controller);
}

inherits(SendCamshot, AutomationModule);

_module = SendCamshot;

// ----------------------------------------------------------------------------
// --- Module instance initialized
// ----------------------------------------------------------------------------

/* Base64 string to array encoding */

function uint6ToB64(nUint6) {

    return nUint6 < 26 ?
        nUint6 + 65 :
        nUint6 < 52 ?
        nUint6 + 71 :
        nUint6 < 62 ?
        nUint6 - 4 :
        nUint6 === 62 ?
        43 :
        nUint6 === 63 ?
        47 :
        65;

}

function base64EncArr(aBytes) {

    var eqLen = (3 - (aBytes.length % 3)) % 3,
        sB64Enc = "";

    for (var nMod3, nLen = aBytes.length, nUint24 = 0, nIdx = 0; nIdx < nLen; nIdx++) {
        nMod3 = nIdx % 3;
        /* Uncomment the following line in order to split the output in lines 76-character long: */
        /*
        if (nIdx > 0 && (nIdx * 4 / 3) % 76 === 0) { sB64Enc += "\r\n"; }
        */
        nUint24 |= aBytes[nIdx] << (16 >>> nMod3 & 24);
        if (nMod3 === 2 || aBytes.length - nIdx === 1) {
            sB64Enc += String.fromCharCode(uint6ToB64(nUint24 >>> 18 & 63), uint6ToB64(nUint24 >>> 12 & 63), uint6ToB64(nUint24 >>> 6 & 63), uint6ToB64(nUint24 & 63));
            nUint24 = 0;
        }
    }

    return eqLen === 0 ?
        sB64Enc :
        sB64Enc.substring(0, sB64Enc.length - eqLen) + (eqLen === 1 ? "=" : "==");

}

SendCamshot.prototype.init = function(config) {
    SendCamshot.super_.prototype.init.call(this, config);

    console.log(JSON.stringify(config));

    const self = this;

    const defaults = {
        metrics: {
            title: self.getInstanceTitle(),
        },
    };

    const overlay = {
        deviceType: 'toggleButton',
        metrics: {
            icon: '',
        },
    };

    const handler = function(command, args) {
        if (command != 'update') {
            const req = {
                url: self.config.screenshotUrl,
                async: true,
                contentType: 'application/octet-stream',
                success: function(resp) {
                    const body = base64EncArr(new Uint8Array(resp.data));

                    const image = "<img alt=\"Camera Screenshot\"" +
                        " src=\"data:image/jpeg;base64," + body + "\">";

                    const message = self.config.emailPrefix && self.config.emailPrefix.length > 0 ?
                        '<p>' + self.config.emailPrefix + '</p>' + image : image;

                    self.addNotification(
                        'mail.notification',
                        message,
                        self.config.email
                    );
                },
                error: function(resp) {
                    console.log("Cannot make request: " + resp.statusText);
                }
            };

            // With Authorization
            if (self.config.user && self.config.password) {
                req.auth = {
                    login: self.config.user,
                    password: self.config.password,
                };
            }

            http.request(req);
        }
    };

    this.vDev = this.controller.devices.create({
        deviceId: 'SendCamshot_' + this.id,
        defaults: defaults,
        overlay: overlay,
        handler: handler,
        moduleId: this.id,
    });
};

SendCamshot.prototype.stop = function() {
    if (this.vDev) {
        this.controller.devices.remove(this.vDev.id);
        this.vDev = null;
    }

    SendCamshot.super_.prototype.stop.call(this);
};

// ----------------------------------------------------------------------------
// --- Module methods
// ----------------------------------------------------------------------------
